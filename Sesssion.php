<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css">
</head>
<body>
    <div class="row" style="background-color: #4cae4c">
        <div class="col-lg-12">
            <div class="col-lg-2">

            </div>
            <div class="col-lg-8">
                <div>
                    <form action="process.php" method="post">

                        <div class="form-group">
                            <label for="fname"> First Name</label>
                            <input type="text" class="form-control" name="fname" id="fname">
                        </div>

                        <label for="lname"> Last Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="lname" id="lname" >
                        </div>

                        <label for="id"> Id</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="id" id="id">
                        </div>

                        <label for="bldgrp"> Blood Group</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="bldgrp" id="bldgrp">
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary">
                        </div>

                    </form>
                </div>
            </div>
        </div>


    </div>
</body>
</html>